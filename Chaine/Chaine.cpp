#include "Chaine.h"

namespace outils
{

	unsigned int strlen(const char* str)
	{
		unsigned int i = 0;

		if (str == NULL){
			return 0;
		}

		while ( str[i] != '\0' )
			i++;
		
		return i;
	}

	char* strcpy(const char* str)
	{
		unsigned int taille = strlen(str);
		char* res = new char[taille+1];
		int i;

		for (i=0; i<taille; i++)
			res[i]=str[i];
		
		res[taille] = str[taille];
		return res;
	}

	bool strsame(const char *str1, const char *str2)
	{
		unsigned int i = 0;

		while ( str1[i] == str2[i] ){
			if ( str1[i] == '\0' )
				return true;
			else
				i++;
		}

		return false;
	}

} // namespace outils


Chaine::Chaine(const char* str)
{
	length_ = outils::strlen(str);
	data_ = outils::strcpy(str);
	// cout << "Chaîne construite" << endl;
}

Chaine::Chaine(const Chaine& str)
{
	data_ = outils::strcpy(str.data_);
	length_ = str.length_;
	// cout << "Chaîne construite" << endl;
}

Chaine&Chaine::operator=(const Chaine& ch)
{
	if ( this != &ch ){
		delete[] this->data_;
		data_ = nullptr;
		length_ = ch.length_;
		if (length_ >0)
			data_=outils::strcpy(ch.data_);
	}

	return *this;
}

Chaine::~Chaine()
{
	delete[] data_;
}

const char* Chaine::c_str() const
{
	return data_;
}

unsigned int Chaine::length() const
{
	return length_;
}

char Chaine::operator[](unsigned int i) const
{
	if ( i < length_ )
		return data_[i];

	else
		return '\0';
}

char&Chaine::operator[](unsigned int i)
{
	if ( i < length_ )
		return data_[i];
}

void Chaine::to_upper()
{
	int i = 0;

	while ( i < length_ ){
		if ( data_[i] > 96 )
			data_[i]-=32;
		i++;
	}
}

void Chaine::to_lower()
{
	unsigned int i = 0;

	while ( i < length_ ){
		if ( data_[i] < 96 && this->data_[i] > 64 )
			data_[i]+=32;
		i++;
	}
}

unsigned int Chaine::find(char c) const
{
	unsigned int res = UINT32_MAX; // UINT2_MAX = le plus grand unsigned int
	// TODO
}

Chaine Chaine::substr(unsigned int begin, unsigned int end) const
{
	// TODO
}

bool operator==(const Chaine& str1, const Chaine& str2)
{
	unsigned int i;

	if (str1.length_ != str2.length_)
		return false;
	
	for(i=0; i < str1.length_; i++)
		if (str1.data_[i] != str2.data_[i])
			return false;
	
	return true;
}

bool operator!=(const Chaine& str1, const Chaine& str2)
{
	return !(str1 == str2);
}

std::ostream& operator<<(std::ostream& o, const Chaine& str)
{
	o << str.c_str();
	return o;
}

std::istream& operator>>(std::istream& i, Chaine& str)
{
	char buffer[1204];
	i>>buffer;
	str = Chaine(buffer);
	return i;
}

