#ifndef CHAINE_H_
#define CHAINE_H_

#include <iostream>

using namespace std;

namespace outils
{

unsigned int strlen(const char* str); // renvoie la longueur de la chaîne str, sans compter le '\0' final
char* strcpy(const char* str); // copie la chaine str sur un emplacement mémoire alloué dynamiquement
bool strsame(const char* str1, const char* str2); // renvoie true ssi les chaînes sont égales.

}



class Chaine final
{

public:
	Chaine(const char* str = nullptr); // constructeur. Prend une chaine de caractères (qui vaut nullptr par défaut)

	Chaine(const Chaine& str); // constructeur par copie. Appelé dans la deuxième instruction du code suivant : Chaine ch1("Une chaine"); Chaine ch2(ch1);

	Chaine& operator=(const Chaine& str); // Opérateur d'affectation. Appelé dans la deuxième instruction du code suivant : Chaine ch1("Une chaine"), ch2("une autre"); ch2 = ch1;

	~Chaine(); // destructeur. Appelé quand la variable sort de portée. Par exemple à la fin du main() ou d'une autre fonction.

	const char* c_str() const; // Méthode renvoyant le const char*, pour garder la compatibilité avec les chaines standard

	unsigned int length() const; // Méthode donnant la longueur de la chaine

	char operator[](unsigned int i) const; // L'opérateur [] permet d'accéder au i-ième élément de la chaine.

	char& operator[](unsigned int i); // Pareil mais sans const : on peut modifier l'objet à l'aide de cette méthode.


	friend bool operator==(const Chaine& str1, const Chaine& str2); // l'opérateur == permet de tester l'égalité de deux chaînes

	friend bool operator!=(const Chaine& str1, const Chaine& str2); // opérateur "différent"

	friend std::ostream& operator<<(std::ostream& o, const Chaine& str); // opérateur de flux : permet d'afficher une chaîne

	friend std::istream& operator>>(std::istream& i, Chaine& str); // opérateur de flux : permet de saisir une chaine au clavier.

	friend Chaine operator+(const Chaine& ch1, const Chaine& ch2); // Ici l'opérateur d'addition permet de concaténer deux chaînes.


	void to_upper(); // Les lettres sont en majuscule
	void to_lower(); // Les lettres sont en minuscule
	unsigned int find(char c) const; // renvoie la position du premier caractère c dans la chaine
	Chaine substr(unsigned int begin, unsigned int end) const; // renvoie un sous-chaine constituée des caractères dans [begin, end[

private:
	char* data_;
	unsigned int length_;
};

#endif
