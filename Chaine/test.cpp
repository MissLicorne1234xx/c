#include "Chaine.h"
#include <gtest/gtest.h>


TEST(Tests_outils, test_strlen)
{
	EXPECT_EQ(outils::strlen("bonjour"), 7);
	EXPECT_EQ(outils::strlen("autre test"), 10);
}

TEST(Tests_outils, test_strsame)
{
	EXPECT_TRUE(outils::strsame("Kappa Keepo","Kappa Keepo"));
	EXPECT_FALSE(outils::strsame("Kappa","Keepo"));
}

TEST(Tests_outils, test_strcpy)
{
	char* str1 = "Voilaaaaaa";
	char* str2 = outils::strcpy(str1);
	EXPECT_TRUE(outils::strsame(str1,str2));
}


TEST(Tests_Chaine, test_constructeur)
{
	Chaine ch1("Bonjour");
	EXPECT_TRUE(outils::strsame(ch1.c_str(), "Bonjour"));
	EXPECT_FALSE(outils::strsame(ch1.c_str(), "Bonjour."));
	EXPECT_EQ(ch1.length(), 7);

	Chaine ch2("");
	EXPECT_EQ(ch2.length(), 0);
}

TEST(Tests_Chaine, test_equal_unequal)
{
	Chaine ch1("Bonjour");
	Chaine ch2("Bonjour");
	Chaine ch3("Plopipl");

	EXPECT_TRUE((ch1 == ch2));
	EXPECT_FALSE((ch1 == ch3));

	EXPECT_TRUE((ch2 != ch3));
	EXPECT_FALSE((ch2 != ch1));
}

TEST(Tests_Chaine, test_constructeur_copie)
{
	Chaine ch1("YOP!");
	Chaine ch2(ch1);

	EXPECT_TRUE((ch1 == ch2));
}

TEST(Tests_Chaine, test_constructeur_affectation_and_output)
{
	Chaine ch1("Salut");
	Chaine ch2 = ch1;

	EXPECT_TRUE((ch1 == ch2));

	cout << ch1 << endl << ch2 << endl;
}

TEST(Tests_Chaine, test_acces_chaine)
{
	Chaine ch1("YOP!");

	EXPECT_EQ(ch1[0], 'Y');
	EXPECT_EQ(ch1[1], 'O');
	EXPECT_EQ(ch1[2], 'P');
	EXPECT_EQ(ch1[3], '!');
	EXPECT_FALSE((ch1[1] == 'K'));
}

TEST(Tests_Chaine, test_modif_chaine)
{
	Chaine ch1("Hola");
	EXPECT_EQ(ch1[2], 'l');

	cout << ch1 << endl;

	ch1[2] = 'P';
	EXPECT_FALSE((ch1[2] == 'l'));
	EXPECT_EQ(ch1[2], 'P');

	cout << ch1 << endl;
}

TEST(Tests_Chaine, test_toupper_tolower)
{
	Chaine ch1("S4luT!<ø3£");

	cout << ch1 << endl;

	ch1.to_upper();
	EXPECT_TRUE(outils::strsame(ch1.c_str(), "S4LUT!<ø3£"));

	cout << ch1 << endl;

	ch1.to_lower();
	EXPECT_TRUE(outils::strsame(ch1.c_str(), "s4lut!<ø3£"));

	cout << ch1 << endl;
}


// TODO : écrire un test pour chaque méthode de la classe Chaine.


// fonction main pour googletest.
int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
